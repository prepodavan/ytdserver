module ytd

go 1.13

replace github.com/Sirupsen/logrus => github.com/sirupsen/logrus v1.4.3-0.20190807103436-de736cf91b92

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/Sirupsen/logrus v0.0.0-00010101000000-000000000000 // indirect
	github.com/essentialkaos/translit v2.0.1+incompatible
	github.com/gin-gonic/gin v1.4.0
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3
	github.com/gorilla/websocket v1.4.1
	github.com/kkdai/youtube v1.0.0 // indirect
	github.com/rylio/ytdl v0.5.1
	github.com/sirupsen/logrus v1.4.2 // indirect
)
