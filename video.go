package main

import (
	"fmt"
	tr "github.com/essentialkaos/translit"
	"github.com/rylio/ytdl"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

type VideoSource struct {
	YID   string
	Path  string
	Title string
}

func (vs *VideoSource) FileName() string {
	return fmt.Sprintf("%s@%s.mp3", vs.YID, vs.Title)
}

func (vs *VideoSource) Download(dir string) (err error) {
	task := &Task{
		VID:    vs.YID,
		Status: "getting meta info",
		Key:    fmt.Sprintf("vid@%s@%d", vs.YID, time.Now().UnixNano()),
		Time:   time.Now().UnixNano(),
	}
	hub <- task
	vid, err := ytdl.GetVideoInfoFromID(vs.YID)
	if err != nil {
		return
	}

	vs.Title = vid.Title
	vs.validate()
	vs.Path = filepath.Join(dir, vs.FileName())
	vidFileName := filepath.Join(os.TempDir(), vs.Title+"."+vid.Formats[0].Extension)

	var file *os.File
	file, err = os.Create(vidFileName)
	if err != nil {
		return
	}

	defer func() {
		file.Close()
		os.Remove(vidFileName)
	}()

	task.Title = vs.Title
	task.Status = "downloading"
	hub <- task
	err = vid.Download(vid.Formats[0], file)
	if err != nil {
		return
	}

	task.Status = "converting"
	hub <- task
	err = vs.convert(vidFileName)
	if err == nil {
		task.Status = "finished"
		hub <- task
	}
	return
}

func (vs *VideoSource) convert(inputFileName string) error {

	return exec.Command(*ffmpeg, "-i", inputFileName, vs.Path, "-y").Run()
}

var words = regexp.MustCompile(`\W`)

func (vs *VideoSource) validate() {
	vs.Title = strings.ToLower(words.ReplaceAllString(tr.EncodeToBGN(vs.Title), "-"))
}
