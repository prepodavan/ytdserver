package main

import (
	"container/list"
	"github.com/gin-gonic/gin"
	"github.com/golang-collections/collections/set"
	"github.com/gorilla/websocket"
	"log"
)

type Task struct {
	VID    string `json:"vId"`
	Status string `json:"status"`
	Key    string `json:"key"`
	Time   int64  `json:"time"`
	Title  string `json:"title,omitempty"`
}

type TaskList struct {
	Tasks []*Task `json:"tasks"`
}

var (
	tasks       = set.New()
	connections = list.New()
	hub         = make(chan *Task, 1000)
	upgrader    = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

func wsAdmin(c *gin.Context) {
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		handle500(c, err)
		return
	}

	sendAll(conn)
	el := connections.PushBack(conn)
	for {
		_, _, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		sendAll(conn)
	}

	connections.Remove(el)
}

func notifier() {
	for {
		t := <-hub
		tasks.Insert(t)
		for conn := connections.Front(); conn != nil; conn = conn.Next() {
			conn.Value.(*websocket.Conn).WriteJSON(t)
		}
	}
}

func sendAll(conn *websocket.Conn) {
	taskList := make([]*Task, 0, tasks.Len())
	tasks.Do(func(i interface{}) {
		taskList = append(taskList, i.(*Task))
	})
	if err := conn.WriteJSON(TaskList{Tasks: taskList}); err != nil {
		log.Println(err)
		return
	}
}
