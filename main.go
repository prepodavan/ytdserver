package main

import (
	"errors"
	"flag"
	"github.com/gin-gonic/gin"
	"strconv"
)

const (
	DefaultPort = 5077
	DefaultHost = "0.0.0.0"
)

var (
	port             = flag.Int("port", DefaultPort, "port to listen on")
	host             = flag.String("host", DefaultHost, "host to listen on")
	musicsDir        = flag.String("musics", "./musics", "dir to store mp3")
	ffmpeg           = flag.String("ffmpeg", "/usr/bin/ffmpeg", "path to ffmpeg executable")
	ErrNoVideoSource = errors.New("no source")
)

func main() {
	flag.Parse()
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.GET("/admin/ws", initMap, wsAdmin)
	base := r.Group("/vid", initMap, find, downloadIfNotFound)
	{
		base.GET("/:id", setHeaders, serveFile)
		base.GET("/:id/title", getTitle)
	}

	go notifier()
	r.Run(*host + ":" + strconv.Itoa(*port))
}
