package main

import (
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const VSKey = "vs"

func getTitle(c *gin.Context) {
	vs, ok := c.Keys[VSKey].(*VideoSource)
	if !ok {
		handle500(c, ErrNoVideoSource)
		return
	}

	c.String(http.StatusOK, vs.Title)
	return
}

func setHeaders(c *gin.Context) {
	vs, ok := c.Keys[VSKey].(*VideoSource)
	if !ok {
		handle500(c, ErrNoVideoSource)
		return
	}

	header := c.Writer.Header()
	header["Content-Type"] = []string{"audio/mpeg"}
	header["Content-Disposition"] = []string{`attachment; filename="` + vs.Title + `.mp3"`}
}

func downloadIfNotFound(c *gin.Context) {
	if _, ok := c.Keys[VSKey]; ok {
		return
	}

	yid := c.Param("id")
	vs := &VideoSource{
		YID: yid,
	}
	if err := vs.Download(*musicsDir); err != nil {
		handle500(c, err)
		return
	}
	c.Keys[VSKey] = vs
}

func serveFile(c *gin.Context) {
	vs, ok := c.Keys[VSKey].(*VideoSource)
	if !ok {
		handle500(c, ErrNoVideoSource)
		return
	}

	file, err := os.Open(vs.Path)
	if err != nil {
		handle500(c, err)
		return
	}
	defer file.Close()

	_, err = io.Copy(c.Writer, file)
	if err != nil {
		handle500(c, err)
		return
	}

	c.Status(http.StatusOK)
	return
}

func handle500(c *gin.Context, e error) {
	log.Println(e)
	c.AbortWithStatus(http.StatusInternalServerError)
}

func find(c *gin.Context) {
	yid := c.Param("id")
	files, err := ioutil.ReadDir(*musicsDir)
	if err != nil {
		handle500(c, err)
		return
	}

	for _, f := range files {
		if strings.HasPrefix(f.Name(), yid) {
			fp, err := filepath.Abs(*musicsDir + "/" + f.Name())
			if err != nil {
				handle500(c, err)
				return
			}
			vs := &VideoSource{
				Path: fp,
				YID:  yid,
			}

			at := strings.Index(vs.Path, "@")
			point := strings.LastIndex(vs.Path, ".mp3")
			vs.Title = vs.Path[at+1 : point]
			c.Keys[VSKey] = vs
			return
		}
	}
	return
}

func initMap(c *gin.Context) {
	if c.Keys == nil {
		c.Keys = make(map[string]interface{})
	}
}
